
<h1 align="center">
  <a href="https://git.io/typing-svg">
    <img src="https://readme-typing-svg.herokuapp.com/?lines=Hello+World&center=true&size=30">
  </a>
</h1>

###  
[![Linkedin Badge](https://img.shields.io/badge/-LinkedIn-blue?style=for-the-badge&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/jlim/)](https://www.linkedin.com/in/guillaume-bertrand-5341b0256/)
[![Website Badge](https://img.shields.io/badge/-COMMING_SOON-588157?style=for-the-badge&logo=Google-Chrome&logoColor=white)](https://www.youtube.com/watch?v=dQw4w9WgXcQ)
[![Gmail Badge](https://img.shields.io/badge/-Gmail-c14438?style=for-the-badge&logo=Gmail&logoColor=white&)](mailto:bertrandguillaume524@gmail.com)

***
# <img src="https://raw.githubusercontent.com/TheDudeThatCode/TheDudeThatCode/master/Assets/Developer.gif" width="45" /> A propos de moi :

- 📍 Toulouse, **_FR_**   
- 🎓 Etudiant en BUT Informatique Parcours Dev à l'université Paul Sabatier  
- ❤️ Passionné de programmation et de nouvelles technologies
- 📘 Je parle Français, Anglais et un peu Espagnol  

***

# <img src="https://raw.githubusercontent.com/TheDudeThatCode/TheDudeThatCode/master/Assets/PC.gif" width="45" /> Ce que j'utilise :


### Je code en :
<div>
<a href="https://developer.mozilla.org/fr/docs/Web/JavaScript"><img title="Javascript" height="50" src="https://upload.wikimedia.org/wikipedia/commons/6/6a/JavaScript-logo.png" ></a>
<a href="https://nodejs.org/"><img title="PHP" height="50" src="https://images-ext-1.discordapp.net/external/-YAXkXppfVSE4_cb16__bG0SoGVbfKH0FsmEAKx-6MA/https/brandslogos.com/wp-content/uploads/images/large/nodejs-logo.png?format=webp&quality=lossless" ></a>
<a href="https://fr.react.dev/"><img title="React" height="50" src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/2300px-React-icon.svg.png" ></a>
<a href="https://www.typescriptlang.org/"><img title="PHP" height="50" src="https://w7.pngwing.com/pngs/74/362/png-transparent-typescript-plain-logo-icon-thumbnail.png" ></a>
<a href="https://phaser.io/"><img title="PHP" height="50" src="https://cdn.phaser.io/images/logo/logo-download.png" ></a>
<a href="https://www.python.org/"><img title="Python" height="50" src="https://s3.dualstack.us-east-2.amazonaws.com/pythondotorg-assets/media/community/logos/python-logo-only.png" ></a>
<a href="https://docs.python.org/fr/3/library/tkinter.html"><img title="Python" height="50" src="https://media.discordapp.net/attachments/1035458415738241025/1221584475096350751/pngwing.com.png?ex=66131c32&is=6600a732&hm=a4723af09acf33529fe995a293936b6d1f01bcd15620f1db8c828797c8c231d2&=&format=webp&quality=lossless&width=339&height=468" ></a>
<a href="https://www.java.com/fr/"><img title="Java" height="50" src="https://upload.wikimedia.org/wikipedia/fr/thumb/2/2e/Java_Logo.svg/550px-Java_Logo.svg.png" ></a>
<a href="https://spring.io/"><img title="Java" height="50" src="https://upload.wikimedia.org/wikipedia/commons/4/44/Spring_Framework_Logo_2018.svg" ></a>
<a href="https://developer.mozilla.org/fr/docs/Web/HTML"><img title="HTML5" height="50" src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/61/HTML5_logo_and_wordmark.svg/2048px-HTML5_logo_and_wordmark.svg.png" ></a>
<a href="https://developer.mozilla.org/fr/docs/Web/CSS"><img title="CSS" height="50" src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/CSS3_logo_and_wordmark.svg/1452px-CSS3_logo_and_wordmark.svg.png" ></a>
<a href="https://fr.wikipedia.org/wiki/Structured_Query_Language"><img title="SQL" height="50" src="https://media.discordapp.net/attachments/1035458415738241025/1221585356135071836/sql-server.png?ex=66131d04&is=6600a804&hm=e94f110540243d168d1296a2072a75128681b29048d549c89078b5f3beb2dd0f&=&format=webp&quality=lossless&width=468&height=468" ></a>
<a href="https://www.oracle.com/fr/database/technologies/appdev/plsql.html#:~:text=PL%2FSQL%20est%20un%20langage,dans%20la%20base%20de%20donn%C3%A9es."><img title="PL-SQL" height="50" src="https://cdn.discordapp.com/attachments/1035458415738241025/1221585911322509392/pl-sql.png?ex=66131d89&is=6600a889&hm=d789d7f9674c44665b7528257d1365817461b6f9b880cf009554bb4fb3e51d1f&" ></a>
<a href="https://www.php.net/"><img title="PHP" height="50" src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/PHP-logo.svg/2560px-PHP-logo.svg.png" ></a>

### Logiciels :
<div>
<a href="https://www.postman.com/"><img title="Postman" height="50" src="https://upload.wikimedia.org/wikipedia/commons/c/c2/Postman_%28software%29.png" ></a>
<a href="https://www.docker.com/"><img title="Docker" height="30" src="https://upload.wikimedia.org/wikipedia/commons/7/79/Docker_%28container_engine%29_logo.png" ></a>
<a href="https://git-scm.com/"><img title="Git" height="50" src="https://git-scm.com/images/logos/downloads/Git-Logo-2Color.png" ></a>
<a href="https://www.eclipse.org/"><img title="Eclipse" height="30" src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d0/Eclipse-Luna-Logo.svg/1280px-Eclipse-Luna-Logo.svg.png" ></a>
<a href="https://code.visualstudio.com/"><img title="VSCode" height="50" src="https://uxwing.com/wp-content/themes/uxwing/download/brands-and-social-media/visual-studio-code-icon.png" ></a>
<a href="https://www.jetbrains.com/fr-fr/"><img title="JetBrain" height="50" src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2d/Tensorflow_logo.svg/449px-Tensorflow_logo.svg.png" ></a>
<a href="https://www.modelio.org/index.htm"><img title="Modelio" height="50" src="https://avatars.githubusercontent.com/u/72009915?s=280&v=4.png" ></a>
<a href="https://www.mysql.com/fr/"><img title="mysql" height="50" src="https://upload.wikimedia.org/wikipedia/fr/thumb/6/62/MySQL.svg/1200px-MySQL.svg.png" ></a>
<a href="https://www.mongodb.com/fr-fr"><img title="mongo" height="50" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/MongoDB_Logo.svg/2560px-MongoDB_Logo.svg.png" ></a>

</div>

### En ce moment j'apprends : 
<a href="https://angular.io/"><img title="mongo" height="50" src="https://cdn4.iconfinder.com/data/icons/logos-and-brands/512/21_Angular_logo_logos-512.png" ></a>





</div>


